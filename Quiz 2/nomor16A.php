<?php

//nomor 16
//<Soal Essay A>

function is_ops($datanya){
	$ops=['+','-','*',':','%'];
	$n=strlen($datanya);
	$hasil=0;
	$i=0;
	$j=0;
	for($i=0;$i<$n;$i++){
		for($j=0;$j<5;$j++){
			if($datanya[$i]==$ops[$j]){
				$hasil=$i;
			}
		}
	}
	return $hasil;
}

function hitung($string_data){
	// $angka=[0,1,2,3,4,5,6,7,8,9];
	$n=strlen($string_data);
	$index_ops=is_ops($string_data);
	
	$simbol=$string_data[$index_ops];

	$angka1=substr($string_data,0,($index_ops));
	$angka2=substr($string_data,($index_ops+1), ($n-$index_ops));


	if($simbol=='+'){
		$hasil= (int)$angka1 + (int)$angka2;
	}

	else if($simbol=='-'){
		$hasil= (int)$angka1 - (int)$angka2;
	}

	else if($simbol=='*'){
		$hasil= (int)$angka1 * (int)$angka2;
	}

	else if($simbol==':'){
		$hasil= (int)$angka1 / (int)$angka2;
	}

	else if($simbol=='%'){
		$hasil= (int)$angka1 % (int)$angka2;
	}

	else{
		$hasil='Data tidak sesuai';
	}

	return $hasil;
}


echo hitung("102*2");
echo "<br>";
echo hitung("2+3");
echo "<br>";
echo hitung("100:25");
echo "<br>";
echo hitung("10%2");
echo "<br>";
echo hitung("99-2");

?>
