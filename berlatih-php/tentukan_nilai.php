<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number<=100 && $number>=85) {
        echo"Nilai Anda $number => Sangat Baik<br>";
    }
    if ($number<=85 && $number>=70) {
        echo"Nilai Anda $number => Baik<br>";
    }
    if ($number<=70 && $number>=60) {
        echo"Nilai Anda $number => Cukup<br>";
    }
    if ($number<=60 && $number>=0) {
        echo"Nilai Anda $number => Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>