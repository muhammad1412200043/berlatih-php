<!DOCTYPE html>
<html>

<head>
    <title>Sanberbook Welcoming Page</title>
</head>

<body>
    <div style="
	background-color: white;
	margin-right: 10px;
	margin-left: 10px;">
        <h1>Selamat Datang 
        <?php
        if ($gender == "male") {
            echo "Tuan ";
        } elseif ($gender == "female") {
            echo "Nyonya ";
        }
        echo $first_name . " " . $last_name;
        ?>
        </h1>
        <h3>Terima Kasih Telah Bergabung di Sanberbook. Social Media Kita Bersama !</h3>
    </div>
    <a href="/">Kembali Ke Landing Page</a>
</body>

</html>